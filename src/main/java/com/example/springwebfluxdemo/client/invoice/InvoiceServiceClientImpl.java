package com.example.springwebfluxdemo.client.invoice;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;
import com.example.springwebfluxdemo.exception.AccountNotFoundException;
import com.example.springwebfluxdemo.exception.DemoServiceException;
import com.example.springwebfluxdemo.exception.DownstreamServiceException;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class InvoiceServiceClientImpl implements InvoiceServiceClient {

    private static final String INVOICE_SERVICE = "invoice-service";

    private final WebClient invoiceServiceWebClient;

    @Override
    public Mono<InvoiceServiceResponse> getInvoices(final String accountNumber) {
        //@formatter:off
        return invoiceServiceWebClient
                .get().uri("/accounts/{accountNumber}", accountNumber)
                .retrieve()
                .bodyToMono(InvoiceServiceResponse.class)
                .onErrorMap(WebClientResponseException.NotFound.class,
                        cause -> new AccountNotFoundException(accountNumber, INVOICE_SERVICE, cause))
                .onErrorMap(cause -> !(cause instanceof DemoServiceException),
                        cause -> new DownstreamServiceException(INVOICE_SERVICE, "Invoice service failure", cause));
        //@formatter:on
    }
}
