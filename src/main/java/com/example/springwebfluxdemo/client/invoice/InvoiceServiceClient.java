package com.example.springwebfluxdemo.client.invoice;

import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;
import reactor.core.publisher.Mono;

public interface InvoiceServiceClient {
    Mono<InvoiceServiceResponse> getInvoices(String accountNumber);
}
