package com.example.springwebfluxdemo.client.billing;

import com.example.springwebfluxdemo.client.billing.dto.BillingServiceResponse;
import reactor.core.publisher.Mono;

public interface BillingServiceClient {
    Mono<BillingServiceResponse> getAccountInfo(String accountNumber);
}
