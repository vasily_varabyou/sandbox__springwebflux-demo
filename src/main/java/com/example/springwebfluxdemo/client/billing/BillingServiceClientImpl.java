package com.example.springwebfluxdemo.client.billing;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import com.example.springwebfluxdemo.client.billing.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.exception.AccountNotFoundException;
import com.example.springwebfluxdemo.exception.DemoServiceException;
import com.example.springwebfluxdemo.exception.DownstreamServiceException;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class BillingServiceClientImpl implements BillingServiceClient {

    private static final String BILLING_SERVICE = "billing-service";

    private final WebClient billingServiceWebClient;

    @Override
    public Mono<BillingServiceResponse> getAccountInfo(final String accountNumber) {
        //@formatter:off
        return billingServiceWebClient
                .get()
                .uri("/accounts/{accountNumber}", accountNumber)
                .retrieve()
                .bodyToMono(BillingServiceResponse.class)
                .onErrorMap(WebClientResponseException.NotFound.class,
                        cause -> new AccountNotFoundException(accountNumber, BILLING_SERVICE, cause))
                .onErrorMap(e -> !(e instanceof DemoServiceException),
                        cause -> new DownstreamServiceException(BILLING_SERVICE, "Billing service failure", cause));
        //@formatter:on
    }
}
