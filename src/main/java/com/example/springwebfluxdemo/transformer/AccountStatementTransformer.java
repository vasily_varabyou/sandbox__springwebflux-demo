package com.example.springwebfluxdemo.transformer;

import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.client.billing.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;

public interface AccountStatementTransformer {
    AccountStatement toAccountStatement(BillingServiceResponse billingServiceResponse,
            InvoiceServiceResponse invoiceServiceResponse);
}
