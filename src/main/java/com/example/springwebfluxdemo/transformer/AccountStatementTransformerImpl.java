package com.example.springwebfluxdemo.transformer;

import static java.util.Optional.ofNullable;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.client.billing.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;

@Service
public class AccountStatementTransformerImpl implements AccountStatementTransformer {
    @Override
    public AccountStatement toAccountStatement(final BillingServiceResponse billingServiceResponse,
            final InvoiceServiceResponse invoiceServiceResponse) {
        //@formatter:off
        return AccountStatement.builder()
                .balance(billingServiceResponse != null ? billingServiceResponse.getBalance() : null)
                .invoices(toInvoices(invoiceServiceResponse))
                .build();
        //@formatter:on
    }

    private List<AccountStatement.InvoiceData> toInvoices(final InvoiceServiceResponse invoiceServiceResponse) {
        //@formatter:off
        return ofNullable(invoiceServiceResponse).map(InvoiceServiceResponse::getInvoices).stream()
                .flatMap(Collection::stream)
                .map(this::toInvoice)
                .collect(Collectors.toList());
        //@formatter:on
    }

    private AccountStatement.InvoiceData toInvoice(final InvoiceServiceResponse.Invoice invoice) {
        //@formatter:off
        return AccountStatement.InvoiceData.builder()
                .invoiceId(invoice.getInvoiceId())
                .issuedAt(invoice.getIssuedAt())
                .totalAmount(invoice.getTotalAmount())
                .build();
        //@formatter:on
    }
}
