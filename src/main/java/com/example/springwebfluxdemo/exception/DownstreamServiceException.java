package com.example.springwebfluxdemo.exception;

import lombok.Getter;

@Getter
public class DownstreamServiceException extends DemoServiceException {
    private final String source;
    public DownstreamServiceException(final String source, final String message, final Throwable cause) {
        super(message, cause);
        this.source = source;
    }
}
