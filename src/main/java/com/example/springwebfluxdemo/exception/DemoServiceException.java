package com.example.springwebfluxdemo.exception;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class DemoServiceException extends Exception {
    public DemoServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
