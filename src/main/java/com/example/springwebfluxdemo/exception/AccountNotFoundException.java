package com.example.springwebfluxdemo.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public class AccountNotFoundException extends DemoServiceException {
    private final String accountNumber;
    private final String source;

    public AccountNotFoundException(final String accountNumber, final String source, final Throwable cause) {
        super("Account not found", cause);
        this.accountNumber = accountNumber;
        this.source = source;
    }
}
