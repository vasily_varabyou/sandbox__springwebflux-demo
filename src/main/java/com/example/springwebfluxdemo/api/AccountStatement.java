package com.example.springwebfluxdemo.api;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
public class AccountStatement {

    BigDecimal balance;
    List<InvoiceData> invoices;

    @Value
    @Jacksonized
    @Builder
    public static class InvoiceData {
        String invoiceId;
        LocalDate issuedAt;
        BigDecimal totalAmount;
    }
}
