package com.example.springwebfluxdemo.config;

import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import static io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.AutoTimer;
import org.springframework.boot.actuate.metrics.web.reactive.client.DefaultWebClientExchangeTagsProvider;
import org.springframework.boot.actuate.metrics.web.reactive.client.MetricsWebClientFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import io.micrometer.core.instrument.MeterRegistry;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClientsConfig {

    @Bean
    public HttpClient httpClient() {
        //@formatter:off
        return HttpClient.create()
                .option(CONNECT_TIMEOUT_MILLIS, 5000)
                .doOnConnected(connection -> connection
                        .addHandlerLast(new ReadTimeoutHandler(15))
                        .addHandlerLast(new WriteTimeoutHandler(15)));
        //@formatter:on
    }

    @Bean
    public WebClient billingServiceWebClient(
            final HttpClient httpClient,
            @Value("${billing.service.baseUrl}") final String baseUrl,
            final MeterRegistry meterRegistry) {
        //@formatter:off
        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(baseUrl)
                .defaultHeader(ACCEPT, APPLICATION_JSON_VALUE)
                .filter(new MetricsWebClientFilterFunction(meterRegistry,
                        new DefaultWebClientExchangeTagsProvider(), "billing-service", AutoTimer.ENABLED))
                .build();
        //@formatter:on
    }

    @Bean
    public WebClient invoiceServiceWebClient(
            final HttpClient httpClient,
            @Value("${invoice.service.baseUrl}") final String baseUrl,
            final MeterRegistry meterRegistry) {
        //@formatter:off
        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(baseUrl)
                .defaultHeader(ACCEPT, APPLICATION_JSON_VALUE)
                .filter(new MetricsWebClientFilterFunction(meterRegistry,
                        new DefaultWebClientExchangeTagsProvider(), "invoice-service", AutoTimer.ENABLED))
                .build();
        //@formatter:on
    }
}
