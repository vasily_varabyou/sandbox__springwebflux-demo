package com.example.springwebfluxdemo.rest;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static com.example.springwebfluxdemo.api.ErrorCode.ACCOUNT_NOT_FOUND;
import static com.example.springwebfluxdemo.api.ErrorCode.DOWNSTREAM_SERVICE_ERROR;
import static com.example.springwebfluxdemo.api.ErrorCode.SERVICE_ERROR;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.example.springwebfluxdemo.api.ServiceError;
import com.example.springwebfluxdemo.exception.AccountNotFoundException;
import com.example.springwebfluxdemo.exception.DownstreamServiceException;

@RestControllerAdvice
public class DemoServiceExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ServiceError> onAccountNotFoundException(final AccountNotFoundException exception) {
        //@formatter:off
        return ResponseEntity.status(NOT_FOUND)
                .body(ServiceError.builder()
                        .source(exception.getSource())
                        .errorCode(ACCOUNT_NOT_FOUND)
                        .errorMessage(format("Account \"%s\" not found", exception.getAccountNumber())).build());
        //@formatter:on
    }

    @ExceptionHandler
    public ResponseEntity<ServiceError> onDownstreamServiceException(final DownstreamServiceException exception) {
        //@formatter:off
        return ResponseEntity.status(INTERNAL_SERVER_ERROR)
                .body(ServiceError.builder()
                        .source(exception.getSource())
                        .errorCode(DOWNSTREAM_SERVICE_ERROR)
                        .errorMessage(exception.getMessage()).build());
        //@formatter:on
    }

    @ExceptionHandler
    public ResponseEntity<ServiceError> onDownstreamServiceException(final Throwable throwable) {
        //@formatter:off
        return ResponseEntity.status(INTERNAL_SERVER_ERROR)
                .body(ServiceError.builder()
                        .errorCode(SERVICE_ERROR)
                        .errorMessage(throwable.getMessage()).build());
        //@formatter:on
    }
}
