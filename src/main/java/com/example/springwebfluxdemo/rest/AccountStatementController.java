package com.example.springwebfluxdemo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.service.AccountStatementService;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/account-statements")
@AllArgsConstructor
public class AccountStatementController {

    private final AccountStatementService service;

    @GetMapping(value = "{accountNumber}")
    public Mono<AccountStatement> get(@PathVariable("accountNumber") final String accountNumber) {
        return service.getAccountStatement(accountNumber);
    }
}
