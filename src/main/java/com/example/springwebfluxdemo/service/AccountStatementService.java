package com.example.springwebfluxdemo.service;

import com.example.springwebfluxdemo.api.AccountStatement;
import reactor.core.publisher.Mono;

public interface AccountStatementService {

    Mono<AccountStatement> getAccountStatement(String accountNumber);
}
