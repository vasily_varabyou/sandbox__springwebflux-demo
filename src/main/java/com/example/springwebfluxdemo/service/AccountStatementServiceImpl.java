package com.example.springwebfluxdemo.service;

import org.springframework.stereotype.Service;
import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.client.billing.BillingServiceClient;
import com.example.springwebfluxdemo.client.invoice.InvoiceServiceClient;
import com.example.springwebfluxdemo.transformer.AccountStatementTransformer;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class AccountStatementServiceImpl implements AccountStatementService {

    private final BillingServiceClient billingServiceClient;
    private final InvoiceServiceClient invoiceServiceClient;
    private final AccountStatementTransformer mapper;

    @Override
    public Mono<AccountStatement> getAccountStatement(final String accountNumber) {
        //@formatter:off
        return billingServiceClient.getAccountInfo(accountNumber)
                .zipWith(
                        invoiceServiceClient.getInvoices(accountNumber),
                        mapper::toAccountStatement);
        //@formatter:on
    }
}
