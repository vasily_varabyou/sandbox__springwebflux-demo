package com.example.springwebfluxdemo.rest;

import static java.util.function.Predicate.isEqual;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static com.example.springwebfluxdemo.util.TestData.aDownstreamServiceException;
import static com.example.springwebfluxdemo.util.TestData.anAccountNotFoundException;
import static com.example.springwebfluxdemo.util.TestData.anAccountStatement;
import static reactor.core.publisher.Mono.error;
import static reactor.core.publisher.Mono.just;
import static reactor.test.StepVerifier.create;

import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.springwebfluxdemo.service.AccountStatementService;

@ExtendWith(MockitoExtension.class)
public class AccountStatementControllerTest {

    private static final String ACCOUNT_NUMBER = "0001";
    @Mock
    private AccountStatementService mockService;
    @InjectMocks
    private AccountStatementController controller;

    private static Stream<Throwable> rethrowableExceptions() {
        return Stream.of(anAccountNotFoundException(ACCOUNT_NUMBER), aDownstreamServiceException());
    }

    @AfterEach
    public void verify() {
        verifyNoMoreInteractions(mockService);
    }

    @Test
    public void shouldReturnAccountStatement() {
        given(mockService.getAccountStatement(ACCOUNT_NUMBER)).willReturn(just(anAccountStatement()));
        //@formatter:off
        create(controller.get(ACCOUNT_NUMBER))
                .expectNext(anAccountStatement())
                .verifyComplete();
        //@formatter:on
        then(mockService).should().getAccountStatement(ACCOUNT_NUMBER);
    }

    @ParameterizedTest
    @MethodSource("rethrowableExceptions")
    public void shouldRethrowExceptionWhenServiceThrows(final Throwable throwable) {
        given(mockService.getAccountStatement(ACCOUNT_NUMBER)).willReturn(error(throwable));
        //@formatter:off
        create(controller.get(ACCOUNT_NUMBER))
                .expectErrorMatches(isEqual(throwable))
                .verify();
        //@formatter:on
        then(mockService).should().getAccountStatement(ACCOUNT_NUMBER);
    }
}