package com.example.springwebfluxdemo.service;

import static java.util.function.Predicate.isEqual;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static com.example.springwebfluxdemo.util.TestData.aBillingServiceResponse;
import static com.example.springwebfluxdemo.util.TestData.aDownstreamServiceException;
import static com.example.springwebfluxdemo.util.TestData.anAccountNotFoundException;
import static com.example.springwebfluxdemo.util.TestData.anAccountStatement;
import static com.example.springwebfluxdemo.util.TestData.anInvoiceServiceResponse;
import static reactor.core.publisher.Mono.error;
import static reactor.core.publisher.Mono.just;
import static reactor.test.StepVerifier.create;

import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.springwebfluxdemo.client.billing.BillingServiceClient;
import com.example.springwebfluxdemo.client.invoice.InvoiceServiceClient;
import com.example.springwebfluxdemo.transformer.AccountStatementTransformer;

@ExtendWith(MockitoExtension.class)
public class AccountStatementServiceTest {

    private static final String ACCOUNT_NUMBER = "0001";

    @Mock
    private BillingServiceClient mockBillingServiceClient;
    @Mock
    private InvoiceServiceClient mockInvoiceServiceClient;
    @Mock
    private AccountStatementTransformer mockAccountStatementTransformer;
    @InjectMocks
    private AccountStatementServiceImpl service;

    private static Stream<Throwable> rethrowableExceptions() {
        return Stream.of(anAccountNotFoundException(ACCOUNT_NUMBER), aDownstreamServiceException());
    }

    @AfterEach
    public void verify() {
        verifyNoMoreInteractions(mockBillingServiceClient, mockInvoiceServiceClient, mockAccountStatementTransformer);
    }

    @Test
    public void shouldGetAccountStatement() {
        given(mockBillingServiceClient.getAccountInfo(ACCOUNT_NUMBER)).willReturn(just(aBillingServiceResponse()));
        given(mockInvoiceServiceClient.getInvoices(ACCOUNT_NUMBER)).willReturn(just(anInvoiceServiceResponse()));
        given(mockAccountStatementTransformer.toAccountStatement(aBillingServiceResponse(), anInvoiceServiceResponse()))
                .willReturn(anAccountStatement());

        //@formatter:off
        create(service.getAccountStatement(ACCOUNT_NUMBER))
                .expectNext(anAccountStatement())
                .verifyComplete();
        //@formatter:on

        then(mockBillingServiceClient).should().getAccountInfo(ACCOUNT_NUMBER);
        then(mockInvoiceServiceClient).should().getInvoices(ACCOUNT_NUMBER);
        then(mockAccountStatementTransformer).should()
                .toAccountStatement(aBillingServiceResponse(), anInvoiceServiceResponse());
    }

    @ParameterizedTest
    @MethodSource("rethrowableExceptions")
    public void shouldRethrowExceptionWhenBillingServiceClientThrows(final Throwable throwable) {
        given(mockBillingServiceClient.getAccountInfo(ACCOUNT_NUMBER)).willReturn(error(throwable));
        given(mockInvoiceServiceClient.getInvoices(ACCOUNT_NUMBER)).willReturn(just(anInvoiceServiceResponse()));

        //@formatter:off
        create(service.getAccountStatement(ACCOUNT_NUMBER))
                .expectErrorMatches(isEqual(throwable))
                .verify();
        //@formatter:on

        then(mockBillingServiceClient).should().getAccountInfo(ACCOUNT_NUMBER);
        then(mockInvoiceServiceClient).should().getInvoices(ACCOUNT_NUMBER);
    }

    @ParameterizedTest
    @MethodSource("rethrowableExceptions")
    public void shouldRethrowExceptionWhenInvoiceServiceClientThrows(final Throwable throwable) {
        given(mockBillingServiceClient.getAccountInfo(ACCOUNT_NUMBER)).willReturn(just(aBillingServiceResponse()));
        given(mockInvoiceServiceClient.getInvoices(ACCOUNT_NUMBER)).willReturn(error(throwable));

        //@formatter:off
        create(service.getAccountStatement(ACCOUNT_NUMBER))
                .expectErrorMatches(isEqual(throwable))
                .verify();
        //@formatter:on

        then(mockBillingServiceClient).should().getAccountInfo(ACCOUNT_NUMBER);
        then(mockInvoiceServiceClient).should().getInvoices(ACCOUNT_NUMBER);
    }

}