package com.example.springwebfluxdemo;

import static java.lang.String.format;
import static java.lang.Thread.sleep;
import static java.util.Collections.singletonList;
import static org.springframework.boot.actuate.metrics.web.reactive.client.WebClientExchangeTags.status;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.boot.actuate.metrics.AutoTimer;
import org.springframework.boot.actuate.metrics.web.reactive.client.MetricsWebClientFilterFunction;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import com.example.springwebfluxdemo.api.AccountStatement;
import io.micrometer.core.instrument.logging.LoggingMeterRegistry;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Flux;
import reactor.netty.http.client.HttpClient;

public class TestClient {

    private static final String BASE_URL = "http://localhost:8090";

    public static void main(String[] args) throws InterruptedException {
        //@formatter:off
        final HttpClient httpClient = HttpClient.create()
                .keepAlive(true)
                .doOnConnected(connection -> connection
                        .addHandlerLast(new ReadTimeoutHandler(30))
                        .addHandlerLast(new WriteTimeoutHandler(30)));
        //@formatter:on

        //@formatter:off
        final LoggingMeterRegistry loggingMeterRegistry = new LoggingMeterRegistry();
        final WebClient client = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(BASE_URL)
                .defaultHeader(ACCEPT, APPLICATION_JSON_VALUE)
                .filter(new MetricsWebClientFilterFunction(loggingMeterRegistry,
                        (request, response, throwable) -> singletonList(status(response, throwable)),
                        "demo-service", AutoTimer.ENABLED))
                .build();
        //@formatter:on

        final Flux<Integer> generator = Flux.generate(() -> 0, (state, sink) -> {
            if (state < 100000)
                sink.next(state + 1);
            else
                sink.complete();
            return state + 1;
        });

        generator.parallel(1000).concatMap(
                i -> client.get().uri(format("/demo-service/account-statements/%d", i)).retrieve()
                        .bodyToMono(AccountStatement.class)).then().block();

        loggingMeterRegistry.close();
        sleep(1000L);
    }
}
