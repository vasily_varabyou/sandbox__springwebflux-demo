package com.example.springwebfluxdemo.util;

import static java.math.BigDecimal.valueOf;
import static java.time.LocalDate.parse;
import static java.util.List.of;

import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.client.billing.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;
import com.example.springwebfluxdemo.exception.AccountNotFoundException;
import com.example.springwebfluxdemo.exception.DownstreamServiceException;

public class TestData {

    public static BillingServiceResponse aBillingServiceResponse() {
        return BillingServiceResponse.builder().balance(valueOf(100.00)).build();
    }

    public static InvoiceServiceResponse anInvoiceServiceResponse() {
        //@formatter:off
        return InvoiceServiceResponse.builder()
                .invoices(of(anInvoice("1"), anInvoice("2"), anInvoice("3"))).build();
        //@formatter:on
    }

    public static InvoiceServiceResponse.Invoice anInvoice(final String id) {
        //@formatter:off
        return InvoiceServiceResponse.Invoice.builder()
                .invoiceId(id)
                .issuedAt(parse("2021-07-24"))
                .totalAmount(valueOf(50.00)).build();
        //@formatter:on
    }

    public static AccountStatement anAccountStatement() {
        //@formatter:off
        return AccountStatement.builder().balance(valueOf(100.00))
                .invoices(of(anInvoiceData("1"), anInvoiceData("2"), anInvoiceData("3"))).build();
        //@formatter:on
    }

    public static AccountStatement.InvoiceData anInvoiceData(final String id) {
        //@formatter:off
        return AccountStatement.InvoiceData.builder()
                .invoiceId(id)
                .issuedAt(parse("2021-07-24"))
                .totalAmount(valueOf(50.00)).build();
        //@formatter:on
    }

    public static AccountNotFoundException anAccountNotFoundException(final String accountNumber) {
        return new AccountNotFoundException(accountNumber, "some-service", null);
    }

    public static DownstreamServiceException aDownstreamServiceException() {
        return new DownstreamServiceException("some-service", "error message", null);
    }
}
