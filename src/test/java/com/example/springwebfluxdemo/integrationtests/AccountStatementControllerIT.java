package com.example.springwebfluxdemo.integrationtests;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.util.ResourceUtils.getFile;
import static com.example.springwebfluxdemo.integrationtests.mocks.BillingServiceMock.expectGetBalanceByAccountNumberAndReturn;
import static com.example.springwebfluxdemo.integrationtests.mocks.BillingServiceMock.verifyGetBalanceByAccountNumber;
import static com.example.springwebfluxdemo.integrationtests.mocks.InvoiceServiceMock.expectGetInvoicesByAccountNumberAndReturn;
import static com.example.springwebfluxdemo.integrationtests.mocks.InvoiceServiceMock.verifyGetInvoicesByAccountNumber;
import static io.restassured.RestAssured.when;
import static io.restassured.path.json.JsonPath.from;

import java.io.FileNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.restassured.RestAssured;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
public class AccountStatementControllerIT {

    private static final String ACCOUNT_NUMBER = "12345678";

    @Value("${local.server.port}")
    private int port;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @BeforeEach
    public void setup() {
        RestAssured.port = port;
    }

    @AfterEach
    public void removeMocks() {
        WireMock.reset();
    }

    @Test
    public void shouldReturnAccountStatement() {
        expectGetBalanceByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-balance.json");
        expectGetInvoicesByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-invoices.json");

        //@formatter:off
        when()
                .get(accountStatementEndpoint(ACCOUNT_NUMBER))
                .then()
                .assertThat().statusCode(HTTP_OK)
                .and().body("", equalTo(expected("account-statement.json")));
        //@formatter:on

        verifyGetBalanceByAccountNumber(ACCOUNT_NUMBER);
        verifyGetInvoicesByAccountNumber(ACCOUNT_NUMBER);
    }

    @Test
    public void shouldReturnNotFoundWhenBillingServiceReturnsNotFound() {
        expectGetBalanceByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_NOT_FOUND, "not-found.json");
        expectGetInvoicesByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-invoices.json");

        //@formatter:off
        when()
                .get(accountStatementEndpoint(ACCOUNT_NUMBER))
                .then()
                .assertThat().statusCode(HTTP_NOT_FOUND)
                .and().body("", equalTo(expected("billing-service-account-not-found.json")));
        //@formatter:on

        verifyGetBalanceByAccountNumber(ACCOUNT_NUMBER);
        verifyGetInvoicesByAccountNumber(ACCOUNT_NUMBER);

    }

    @Test
    public void shouldReturnNotFoundWhenInvoiceServiceReturnsNotFound() {
        expectGetBalanceByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-balance.json");
        expectGetInvoicesByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_NOT_FOUND, "not-found.json");

        //@formatter:off
        when()
                .get(accountStatementEndpoint(ACCOUNT_NUMBER))
                .then()
                .assertThat().statusCode(HTTP_NOT_FOUND)
                .and().body("", equalTo(expected("invoice-service-account-not-found.json")));
        //@formatter:on

        verifyGetBalanceByAccountNumber(ACCOUNT_NUMBER);
        verifyGetInvoicesByAccountNumber(ACCOUNT_NUMBER);

    }

    @Test
    public void shouldReturnInternalServerErrorWhenBillingServiceReturnsInternalServerError() {
        expectGetBalanceByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_INTERNAL_ERROR, "internal-server-error.json");
        expectGetInvoicesByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-invoices.json");

        //@formatter:off
        when()
                .get(accountStatementEndpoint(ACCOUNT_NUMBER))
                .then()
                .assertThat().statusCode(HTTP_INTERNAL_ERROR)
                .and().body("", equalTo(expected("billing-service-internal-server-error.json")));
        //@formatter:on

        verifyGetBalanceByAccountNumber(ACCOUNT_NUMBER);
        verifyGetInvoicesByAccountNumber(ACCOUNT_NUMBER);

    }

    @Test
    public void shouldReturnInternalServerErrorWhenInvoiceServiceReturnsInternalServerError() {
        expectGetBalanceByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_OK, "account-balance.json");
        expectGetInvoicesByAccountNumberAndReturn(ACCOUNT_NUMBER, HTTP_INTERNAL_ERROR, "internal-server-error.json");

        //@formatter:off
        when()
                .get(accountStatementEndpoint(ACCOUNT_NUMBER))
                .then()
                .assertThat().statusCode(HTTP_INTERNAL_ERROR)
                .and().body("", equalTo(expected("invoice-service-internal-server-error.json")));
        //@formatter:on

        verifyGetBalanceByAccountNumber(ACCOUNT_NUMBER);
        verifyGetInvoicesByAccountNumber(ACCOUNT_NUMBER);

    }

    private String accountStatementEndpoint(final String accountNumber) {
        return contextPath + "/account-statements/" + accountNumber;
    }

    private <T> T expected(final String expectedResponseName) {
        try {
            return from(getFile("classpath:expected/" + expectedResponseName)).get();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
