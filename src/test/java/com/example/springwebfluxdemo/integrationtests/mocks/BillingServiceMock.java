package com.example.springwebfluxdemo.integrationtests.mocks;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.http.MimeType.JSON;
import static wiremock.org.apache.http.HttpHeaders.CONTENT_TYPE;

public class BillingServiceMock {

    public static void expectGetBalanceByAccountNumberAndReturn(final String accountNumber, final int httpStatus,
            final String responseBody) {
        //@formatter:off
        givenThat(get("/billing-service/accounts/" + accountNumber)
                .willReturn(aResponse()
                        .withStatus(httpStatus)
                        .withHeader(CONTENT_TYPE, JSON.toString())
                        .withBodyFile("mocks/billing-service/" + responseBody)));
        //@formatter:on
    }

    public static void verifyGetBalanceByAccountNumber(final String accountNumber) {
        verify(getRequestedFor(urlEqualTo("/billing-service/accounts/" + accountNumber)));
    }
}
