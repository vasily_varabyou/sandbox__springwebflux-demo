package com.example.springwebfluxdemo.client.invoice;

import static java.util.function.Predicate.isEqual;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static com.example.springwebfluxdemo.util.TestData.anInvoiceServiceResponse;
import static reactor.core.publisher.Mono.error;
import static reactor.core.publisher.Mono.just;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import com.example.springwebfluxdemo.client.invoice.dto.InvoiceServiceResponse;
import com.example.springwebfluxdemo.exception.AccountNotFoundException;
import com.example.springwebfluxdemo.exception.DownstreamServiceException;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceClientTest {
    private static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";

    @Mock
    private WebClient mockWebClient;
    @Mock
    private WebClient.RequestHeadersUriSpec<?> mockRequestUriSpec;
    @Mock
    private WebClient.ResponseSpec mockResponseSpec;
    @InjectMocks
    private InvoiceServiceClientImpl client;

    @AfterEach
    public void verify() {
        verifyNoMoreInteractions(mockWebClient, mockRequestUriSpec, mockResponseSpec);
    }

    @Test
    public void shouldGetAccountInfo() {
        given(mockWebClient.get()).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.uri("/accounts/{accountNumber}", ACCOUNT_NUMBER)).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.retrieve()).willReturn(mockResponseSpec);
        given(mockResponseSpec.bodyToMono(InvoiceServiceResponse.class)).willReturn(just(anInvoiceServiceResponse()));

        //@formatter:off
        StepVerifier.create(client.getInvoices(ACCOUNT_NUMBER))
                .expectNext(anInvoiceServiceResponse())
                .verifyComplete();
        //@formatter:on

        then(mockWebClient).should().get();
        then(mockRequestUriSpec).should().uri("/accounts/{accountNumber}", ACCOUNT_NUMBER);
        then(mockRequestUriSpec).should().retrieve();
        then(mockResponseSpec).should().bodyToMono(InvoiceServiceResponse.class);
    }

    @Test
    public void shouldReturnAccountNotFoundExceptionWhenWebClientThrowsNotFound() {
        final WebClientResponseException cause =
                WebClientResponseException.create(NOT_FOUND.value(), "Not found", null, null, null);
        final AccountNotFoundException expectedException =
                new AccountNotFoundException(ACCOUNT_NUMBER, "invoice-service", cause);

        given(mockWebClient.get()).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.uri("/accounts/{accountNumber}", ACCOUNT_NUMBER)).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.retrieve()).willReturn(mockResponseSpec);
        given(mockResponseSpec.bodyToMono(InvoiceServiceResponse.class)).willReturn(error(cause));

        //@formatter:off
        StepVerifier.create(client.getInvoices(ACCOUNT_NUMBER))
                .expectErrorMatches(isEqual(expectedException))
                .verify();
        //@formatter:on

        then(mockWebClient).should().get();
        then(mockRequestUriSpec).should().uri("/accounts/{accountNumber}", ACCOUNT_NUMBER);
        then(mockRequestUriSpec).should().retrieve();
        then(mockResponseSpec).should().bodyToMono(InvoiceServiceResponse.class);
    }

    @Test
    public void shouldReturnDownstreamServiceExceptionWhenWebClientFails() {
        final WebClientResponseException cause =
                WebClientResponseException.create(INTERNAL_SERVER_ERROR.value(), "Internal error", null, null, null);
        final DownstreamServiceException expectedException =
                new DownstreamServiceException("invoice-service", "Invoice service failure", cause);

        given(mockWebClient.get()).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.uri("/accounts/{accountNumber}", ACCOUNT_NUMBER)).willAnswer(i -> mockRequestUriSpec);
        given(mockRequestUriSpec.retrieve()).willReturn(mockResponseSpec);
        given(mockResponseSpec.bodyToMono(InvoiceServiceResponse.class)).willReturn(error(cause));

        //@formatter:off
        StepVerifier.create(client.getInvoices(ACCOUNT_NUMBER))
                .expectErrorMatches(isEqual(expectedException))
                .verify();
        //@formatter:on

        then(mockWebClient).should().get();
        then(mockRequestUriSpec).should().uri("/accounts/{accountNumber}", ACCOUNT_NUMBER);
        then(mockRequestUriSpec).should().retrieve();
        then(mockResponseSpec).should().bodyToMono(InvoiceServiceResponse.class);
    }
}
