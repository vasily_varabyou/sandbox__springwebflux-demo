package com.example.springwebfluxdemo.mock.dto;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
public class BillingServiceResponse {
    BigDecimal balance;
}
