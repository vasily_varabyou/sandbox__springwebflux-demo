package com.example.springwebfluxdemo.mock;

import static java.lang.String.format;
import static java.math.BigDecimal.valueOf;
import static java.time.Duration.ofSeconds;
import static java.time.LocalDate.now;

import static reactor.core.publisher.Mono.just;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.springwebfluxdemo.mock.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.mock.dto.InvoiceServiceResponse;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping
public class MockController {

    @GetMapping(value = "/billing-service/accounts/{accountNumber}")
    public Mono<BillingServiceResponse> getAccountBalance(@PathVariable("accountNumber") final String accountNumber) {
        return just(aBillingServiceResponse()).delayElement(ofSeconds(5));
    }

    @GetMapping(value = "/invoice-service/accounts/{accountNumber}")
    public Mono<InvoiceServiceResponse> getInvoices(@PathVariable("accountNumber") final String accountNumber) {
        return just(anInvoiceServiceResponse()).delayElement(ofSeconds(5));
    }

    private BillingServiceResponse aBillingServiceResponse() {
        return BillingServiceResponse.builder().balance(valueOf(100)).build();
    }

    private InvoiceServiceResponse anInvoiceServiceResponse() {
        //@formatter:off
        return InvoiceServiceResponse.builder().invoices(
                IntStream.range(1, 101).mapToObj(this::anInvoice).collect(Collectors.toList())).build();
        //@formatter:on
    }

    private InvoiceServiceResponse.Invoice anInvoice(final int no) {
        return InvoiceServiceResponse.Invoice.builder().invoiceId(format("INV%08d", no)).issuedAt(now())
                .totalAmount(valueOf(10.00)).build();
    }
}
