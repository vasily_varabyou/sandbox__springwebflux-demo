package com.example.springwebfluxdemo.transformer;

import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.dto.BillingServiceResponse;
import com.example.springwebfluxdemo.dto.InvoiceServiceResponse;

public interface AccountStatementTransformer {
    AccountStatement toAccountStatement(BillingServiceResponse billingServiceResponse,
            InvoiceServiceResponse invoiceServiceResponse);
}
