package com.example.springwebfluxdemo.service;

import com.example.springwebfluxdemo.api.AccountStatement;

public interface AccountStatementService {

    AccountStatement getAccountStatement(String accountNumber);
}
