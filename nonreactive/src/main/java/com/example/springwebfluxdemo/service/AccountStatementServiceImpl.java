package com.example.springwebfluxdemo.service;

import org.springframework.stereotype.Service;
import com.example.springwebfluxdemo.api.AccountStatement;
import com.example.springwebfluxdemo.client.BillingServiceClient;
import com.example.springwebfluxdemo.client.InvoiceServiceClient;
import com.example.springwebfluxdemo.transformer.AccountStatementTransformer;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AccountStatementServiceImpl implements AccountStatementService {

    private final BillingServiceClient billingServiceClient;
    private final InvoiceServiceClient invoiceServiceClient;
    private final AccountStatementTransformer transformer;

    @Override
    public AccountStatement getAccountStatement(final String accountNumber) {
        return transformer.toAccountStatement(billingServiceClient.getAccountInfo(accountNumber),
                invoiceServiceClient.getInvoices(accountNumber));
    }
}
