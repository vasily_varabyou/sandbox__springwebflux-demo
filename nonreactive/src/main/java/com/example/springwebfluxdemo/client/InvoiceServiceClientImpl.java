package com.example.springwebfluxdemo.client;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import com.example.springwebfluxdemo.dto.InvoiceServiceResponse;

@Component
public class InvoiceServiceClientImpl implements InvoiceServiceClient {
    private final String baseUrl;
    private final RestOperations restOperations;

    public InvoiceServiceClientImpl(@Value("${invoice.service.baseUrl}") final String baseUrl,
            final RestTemplateBuilder restTemplateBuilder) {
        this.baseUrl = baseUrl;
        this.restOperations = restTemplateBuilder.build();
    }

    @Override
    public InvoiceServiceResponse getInvoices(final String accountNumber) {
        return restOperations.getForObject(
                fromHttpUrl(baseUrl).pathSegment("accounts", "{accountNumber}").buildAndExpand(accountNumber).toUri(),
                InvoiceServiceResponse.class);
    }
}
