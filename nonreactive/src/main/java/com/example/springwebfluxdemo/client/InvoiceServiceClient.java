package com.example.springwebfluxdemo.client;

import com.example.springwebfluxdemo.dto.InvoiceServiceResponse;

public interface InvoiceServiceClient {
    InvoiceServiceResponse getInvoices(String accountNumber);
}
