package com.example.springwebfluxdemo.client;

import com.example.springwebfluxdemo.dto.BillingServiceResponse;

public interface BillingServiceClient {
    BillingServiceResponse getAccountInfo(String accountNumber);
}
