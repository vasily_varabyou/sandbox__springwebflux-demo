package com.example.springwebfluxdemo.client;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import com.example.springwebfluxdemo.dto.BillingServiceResponse;

@Component
public class BillingServiceClientImpl implements BillingServiceClient {

    private final String baseUrl;
    private final RestOperations restOperations;

    public BillingServiceClientImpl(@Value("${billing.service.baseUrl}") final String baseUrl,
            final RestTemplateBuilder restTemplateBuilder) {
        this.baseUrl = baseUrl;
        this.restOperations = restTemplateBuilder.build();
    }

    @Override
    public BillingServiceResponse getAccountInfo(final String accountNumber) {
        return restOperations
                .getForObject(fromHttpUrl(baseUrl).pathSegment("accounts", "{accountNumber}").buildAndExpand(accountNumber).toUri(),
                        BillingServiceResponse.class);
    }
}
