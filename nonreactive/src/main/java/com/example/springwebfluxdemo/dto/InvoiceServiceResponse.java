package com.example.springwebfluxdemo.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
public class InvoiceServiceResponse {

    List<Invoice> invoices;

    @Value
    @Jacksonized
    @Builder
    public static class Invoice {
        String invoiceId;
        LocalDate issuedAt;
        BigDecimal totalAmount;
    }
}
