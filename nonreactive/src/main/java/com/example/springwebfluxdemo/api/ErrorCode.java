package com.example.springwebfluxdemo.api;

public enum ErrorCode {
    ACCOUNT_NOT_FOUND,
    DOWNSTREAM_SERVICE_ERROR,
    SERVICE_ERROR;
}
