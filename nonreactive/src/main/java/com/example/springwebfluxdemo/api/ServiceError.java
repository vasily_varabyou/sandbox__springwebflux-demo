package com.example.springwebfluxdemo.api;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Jacksonized
@Builder
public class ServiceError {
    ErrorCode errorCode;
    String errorMessage;
    String source;
}
